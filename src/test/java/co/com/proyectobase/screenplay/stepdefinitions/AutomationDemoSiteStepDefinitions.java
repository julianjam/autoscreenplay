package co.com.proyectobase.screenplay.stepdefinitions;

import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.questions.ElMensajeDeVerificacion;
import co.com.proyectobase.screenplay.tasks.Abrir;
import co.com.proyectobase.screenplay.tasks.Diligenciar;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;

import net.serenitybdd.screenplay.Actor;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class AutomationDemoSiteStepDefinitions {

	@Managed(driver="chrome")
	private WebDriver hisBrowser;
	private Actor carlos = Actor.named("Carlos");
	
	@Before
	public void configuracionInicial(){
		carlos.can(BrowseTheWeb.with(hisBrowser));	
	}
		
	@Dado("^que Carlos quiere acceder a la Web Automation Demo Site$")
	public void queCarlosQuiereAccederALaWebAutomationDemoSite() {
	  carlos.wasAbleTo(Abrir.LaPaginaAutomationTestingRegister());
	}
	
	@Cuando("^el realiza el registro en la página$")
	public void elRealizaElRegistroEnLaPágina(DataTable tabla){
		List<List<String>> data = tabla.raw();
		for(int i=1; i<data.size(); i++) {
			carlos.wasAbleTo(Diligenciar.CamposDeRegistro(data ,i));
			try {
				Thread .sleep(5000);
			}catch(InterruptedException e) {}
		}
	}

	@Entonces("^el verifica que se carga la pantalla con texto (.*)$")
	public void elVerificaQueSeCargaLaPantallaConTextoDoubleClickOnEditIconToEDITTheTableRow(String textoEsperado) {
		carlos.should(seeThat(ElMensajeDeVerificacion.es(), equalTo(textoEsperado)));
	}
	
}
