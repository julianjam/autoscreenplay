package co.com.proyectobase.screenplay.tasks;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;

import co.com.proyectobase.screenplay.userinterface.AutomationTestingRegister;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class Diligenciar implements Task{
	
	private String firstName;
	private String lastName;
	private String address;
	private String email;
	private String phone;
	private String gender;
	private String hobbies;
	private String languages;
	private String skills;
	private String country;
	private String sCountry;
	private String year;
	private String month;
	private String day;
	private String password;
	private String confPassword;
	
	

	public Diligenciar(String firstName, String lastName, String address, String email, String phone, String gender,
			String hobbies, String languages, String skills, String country, String sCountry, String year, String month,
			String day, String password, String confPassword) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.email = email;
		this.phone = phone;
		this.gender = gender;
		this.hobbies = hobbies;
		this.languages = languages;
		this.skills = skills;
		this.country = country;
		this.sCountry = sCountry;
		this.year = year;
		this.month = month;
		this.day = day;
		this.password = password;
		this.confPassword = confPassword;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(Enter.theValue(firstName).into(AutomationTestingRegister.CAMPO_FIRST_NAME));
		actor.attemptsTo(Enter.theValue(lastName).into(AutomationTestingRegister.CAMPO_LAST_NAME));
		actor.attemptsTo(Enter.theValue(address).into(AutomationTestingRegister.CAMPO_LAST_ADDRESS));
		actor.attemptsTo(Enter.theValue(email).into(AutomationTestingRegister.CAMPO_LAST_EMAIL));
		actor.attemptsTo(Enter.theValue(phone).into(AutomationTestingRegister.CAMPO_LAST_PHONE));
		
		AutomationTestingRegister.CAMPO_GENERO.resolveFor(actor)
		.find(By.xpath("//input[contains(@value,'" + gender + "')]")).click();
		
		List<String> listaHobbies = Arrays.asList(hobbies.split(","));
		for(int i=0; i<listaHobbies.size(); i++) {
			AutomationTestingRegister.CAMPO_HOBBIES.resolveFor(actor)
			.find(By.xpath("//input[contains(@value,'" + listaHobbies.get(i).trim() + "')]")).click();
		}
		
		actor.attemptsTo(Click.on(AutomationTestingRegister.CAMPO_IDIOMAS));
		List<String> idiomas = Arrays.asList(languages.split(","));
		
		for(int i=0; i<idiomas.size(); i++) {
					
			AutomationTestingRegister.LISTA_IDIOMAS.resolveFor(actor)
			.find(By.xpath("//a[contains(text(),'" + idiomas.get(i).trim() + "')]")).click();
		}
		
		actor.attemptsTo(Click.on(AutomationTestingRegister.LABEL_HABILIDADES));
		
		actor.attemptsTo(Click.on(AutomationTestingRegister.LISTA_HABILIDADES));
		AutomationTestingRegister.LISTA_HABILIDADES.resolveFor(actor)
		.find(By.xpath("//option[contains(text(),'" + skills + "')]")).click();
		
		actor.attemptsTo(Click.on(AutomationTestingRegister.PRIMERA_LISTA_PAISES));
		AutomationTestingRegister.PRIMERA_LISTA_PAISES.resolveFor(actor)
		.find(By.xpath("//option[contains(text(),'" + country + "')]")).click();
		
		actor.attemptsTo(Click.on(AutomationTestingRegister.DESPLEGABLE_SEGUNDA_LISTA_PAISES));
		AutomationTestingRegister.RESULTADO_SEGUNDA_LISTA_PAISES.resolveFor(actor)
		.find(By.xpath("//li[contains(text(),'" + sCountry + "')]")).click();
		
		actor.attemptsTo(Click.on(AutomationTestingRegister.CAMPO_ANNIO));
		AutomationTestingRegister.CAMPO_ANNIO.resolveFor(actor)
		.find(By.xpath("//option[contains(@value,'" + year + "')]")).click();
		
		actor.attemptsTo(Click.on(AutomationTestingRegister.CAMPO_MES));
		AutomationTestingRegister.CAMPO_MES.resolveFor(actor)
		.find(By.xpath("//option[contains(@value,'" + month + "')]")).click();
		
		actor.attemptsTo(Click.on(AutomationTestingRegister.CAMPO_DIA));
		AutomationTestingRegister.CAMPO_DIA.resolveFor(actor)
		.find(By.xpath("//option[starts-with(@value,'" + day + "')]")).click();
		
		actor.attemptsTo(Enter.theValue(password).into(AutomationTestingRegister.CAMPO_PASSWORD));
		
		actor.attemptsTo(Enter.theValue(confPassword).into(AutomationTestingRegister.CAMPO_CONFIRMACION_PASSWORD));
		
		actor.attemptsTo(Click.on(AutomationTestingRegister.BOTON_SUBMIT));
	}
	

	public static Diligenciar CamposDeRegistro(List<List<String>> data, int id) {
		
		return Tasks.instrumented(Diligenciar.class, data.get(id).get(0).trim()
												   , data.get(id).get(1).trim()
												   , data.get(id).get(2).trim()
												   , data.get(id).get(3).trim()
												   , data.get(id).get(4).trim()
												   , data.get(id).get(5).trim()
												   , data.get(id).get(6).trim()
												   , data.get(id).get(7).trim()
												   , data.get(id).get(8).trim()
												   , data.get(id).get(9).trim()
												   , data.get(id).get(10).trim()
												   , data.get(id).get(11).trim()
												   , data.get(id).get(12).trim()
												   , data.get(id).get(13).trim()
												   , data.get(id).get(14).trim()
												   , data.get(id).get(15).trim());								   
	}
}
