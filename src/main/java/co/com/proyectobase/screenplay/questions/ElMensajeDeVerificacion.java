package co.com.proyectobase.screenplay.questions;

import co.com.proyectobase.screenplay.userinterface.AutomationTestingRegister;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class ElMensajeDeVerificacion implements Question<String>{

	public static ElMensajeDeVerificacion es() {
		
		return new ElMensajeDeVerificacion();		
	}

	@Override
	public String answeredBy(Actor actor) {

		return Text.of(AutomationTestingRegister.AREA_CONFIRMACION).viewedBy(actor).asString();
	}

}
