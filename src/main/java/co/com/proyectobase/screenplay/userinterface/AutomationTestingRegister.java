package co.com.proyectobase.screenplay.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://demo.automationtesting.in/Register.html")
public class AutomationTestingRegister extends PageObject{

	public static final Target CAMPO_FIRST_NAME = Target.the("El campo donde se ingresa el nombre")
												.located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[1]/div[1]/input"));
	
	public static final Target CAMPO_LAST_NAME = Target.the("El campo donde se ingresa el apellido")
			.located(By.xpath("//input[@class=\'form-control ng-pristine ng-untouched ng-invalid ng-invalid-required\']"));
	
	public static final Target CAMPO_LAST_ADDRESS = Target.the("El campo donde se ingresa la dirección")
			.located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[2]/div/textarea"));
	public static final Target CAMPO_LAST_EMAIL = Target.the("El campo donde se ingresa el email")
			.located(By.xpath("//*[@id=\"eid\"]/input"));
	public static final Target CAMPO_LAST_PHONE = Target.the("El campo donde se ingresa el número telefónico")
			.located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[4]/div/input"));
	public static final Target CAMPO_GENERO = Target.the("El campo donde se ingresa el género")
			.located(By.name("radiooptions"));
	public static final Target CAMPO_HOBBIES = Target.the("El campo donde se seleccionan los hobbies")
			.located(By.id("checkbox1"));
	public static final Target CAMPO_IDIOMAS = Target.the("campo de idiomas")
			.located(By.id("msdd"));
	public static final Target LISTA_IDIOMAS = Target.the("campo de idiomas")
			.located(org.openqa.selenium.By.xpath("//*[@id=\"basicBootstrapForm\"]/div[7]/div/multi-select/div[2]/ul"));
		
	public static final Target LABEL_HABILIDADES = Target.the("Label de habilidades")
			.located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[8]/label"));
	public static final Target LISTA_HABILIDADES = Target.the("Lista de habilidades")
			.located(By.id("Skills"));
	public static final Target PRIMERA_LISTA_PAISES = Target.the("Primera lista de paises")
			.located(By.id("countries"));
	public static final Target DESPLEGABLE_SEGUNDA_LISTA_PAISES = Target.the("Desplegable Segunda lista de paises")
			.located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[10]/div/span/span[1]/span"));
	public static final Target RESULTADO_SEGUNDA_LISTA_PAISES = Target.the("Segunda lista de paises")
			.located(By.id("select2-country-results"));
	public static final Target CAMPO_ANNIO = Target.the("El campo donde se ingresa el año de nacimiento")
			.located(By.id("yearbox"));
	public static final Target CAMPO_MES = Target.the("El campo donde se ingresa el mes de nacimiento")
			.located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[11]/div[2]/select"));
	public static final Target CAMPO_DIA = Target.the("El campo donde se ingresa el día de nacimiento")
			.located(By.id("daybox"));
	public static final Target CAMPO_PASSWORD = Target.the("El campo donde se ingresa el password")
			.located(By.id("firstpassword"));
	public static final Target CAMPO_CONFIRMACION_PASSWORD = Target.the("El campo donde se ingresa la confirmación del password")
			.located(By.id("secondpassword"));
	public static final Target BOTON_SUBMIT = Target.the("El boton para guardar los datos de registro")
			.located(By.id("submitbtn"));
	
	public static final Target AREA_CONFIRMACION = Target.the("El area para verificar que se registró correctamente")
			.located(By.xpath("/html/body/section/div[1]/div/div[2]/h4[1]"));
	
	
}